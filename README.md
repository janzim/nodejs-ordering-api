# NodeJS Ordering API

Ordering API using NodeJS Express, where user can order food, get its preperation status and collect it once it is done.

# Endpoints

## Root
Friendly message :-)
```
GET /
```

## Post an order
```
POST /v1/order
```

**Input**
```
{
    "type": "Coffe",
    "customer": "janzim",
    "quantity": "1"
}
```
Accepted types: Coffee, Burger, Pasta

**Response**
```
{
    "success": true,
    "ordernr": 0,
    "timetofinish": 30
}
```

## Get an order
```
GET /v1/order/:orderid
```

**Response**
```
{
    "success": true,
    "status": "in progress",
    "type": "coffee",
    "custome": "janzim",
    "quantity": "1",
    "doneIn": 19.719
}
```

## Collect an order
```
PUT /v1/collect/:orderid
```

**Response**
```
{
    "success": true,
    "status": "collected",
    "type": "coffee",
    "custome": "janzim",
    "quantity": "1"
}
```

## Delete a collected order
```
DELETE /v1/collect/:orderid
```

**Response**
```
{
    "success": true,
    "status": "deleted",
    "type": "coffee",
    "custome": "janzim",
    "quantity": "1"
}
```

