module.exports = class Order {
	constructor(id, status, type, customer, quantity, cookingTime) {
		this.id = id;
		this.status = status;
		this.type = type;
		this.customer = customer;
		this.quantity = quantity;
		this.cookingTime = cookingTime;
		this.startTime = new Date().getTime();
	}

	isDone() {
		return this.timeLeft() == 0;
	}

	timeLeft() {
		const time =
			(this.cookingTime - (new Date().getTime() - this.startTime)) / 1000;

		return time > 0 ? time : 0;
	}

	getStatus() {
		if (this.status === 'collected') {
			return this.status;
		}
		if (this.isDone()) {
			return 'done';
		} else {
			return 'in progress';
		}
	}
};
