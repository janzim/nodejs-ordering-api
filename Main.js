const express = require('express');
const { PORT = 3000 } = process.env;
const Order = require('./Order');
const app = express();
app.use(express.json());

const cookingTime = 30000; // in milliseconds
let ordernr = 0;

let orders = [];

const foodType = {
	BURGER: 'burger',
	PASTA: 'pasta',
	COFFEE: 'coffee',
};

const isValidFood = (food) => {
	if (
		food === foodType.BURGER ||
		food === foodType.PASTA ||
		food === foodType.COFFEE
	) {
		return true;
	}
	return false;
};

// Normal endpoint
app.get('/', (req, res) => {
	return res.status(200).send('I am an ordering API, ORDER SOMETHING! :D');
});

// Post an order
app.post('/v1/order', (req, res) => {
	let { type, customer, quantity } = req.body;
	type = type.toLowerCase();

	if (!isValidFood(type)) {
		return res.status(404).json({
			succcess: false,
			message: `We don't serve "${type}" here :/`,
		});
	} else {
		orders.push(
			new Order(
				ordernr,
				'in progress',
				type,
				customer,
				quantity,
				cookingTime
			)
		);
		return res.status(201).json({
			success: true,
			ordernr: ordernr++,
			timetofinish: cookingTime / 1000,
		});
	}
});

// Get a specific order
app.get('/v1/order/:orderid', (req, res) => {
	const { orderid } = req.params;
	const order = orders.filter((o) => o.id == orderid)[0];

	if (order) {
		return res.status(200).json({
			success: true,
			status: order.getStatus(),
			type: order.type,
			custome: order.customer,
			quantity: order.quantity,
			doneIn: order.timeLeft(),
		});
	} else {
		return res.status(404).json({
			success: false,
			error: `No order with number "${orderid}" exists!`,
		});
	}
});

// Update an order to done
app.put('/v1/collect/:orderid', (req, res) => {
	const { orderid } = req.params;
	const order = orders.filter((o) => o.id == orderid)[0];

	if (order.isDone()) {
		order.status = 'collected';
		return res.status(200).json({
			success: true,
			status: order.getStatus(),
			type: order.type,
			customer: order.customer,
			quantity: order.quantity,
		});
	} else {
		return res.status(428).json({
			success: false,
			message: `Order ${orderid} still has ${order.timeLeft()} seconds left until done!`,
		});
	}
});

// Delete an order
app.delete('/v1/collect/:orderid', (req, res) => {
	const { orderid } = req.params;
	const order = orders.filter((o) => o.id == orderid)[0];

	if (order.status === 'collected') {
		orders = orders.filter((o) => o.id != orderid);
		return res.status(200).json({
			success: true,
			status: 'deleted',
			type: order.type,
			customer: order.customer,
			quantity: order.quantity,
		});
	} else {
		return res.status(404).json({
			success: false,
			message: `Order is not done or has not been collected yet! Time left ${order.timeLeft()}`,
		});
	}
});

app.listen(PORT, () => console.log('Server started on port ' + PORT));
